cmake_minimum_required(VERSION 3.16)

project(Server)

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif ()
string(TOLOWER ${CMAKE_BUILD_TYPE} BUILD_TYPE_DIR)

if (NOT PLATFORM_ID)
    message(FATAL "you must specify a platform ID: -DPLATFORM_ID=linux1fedora31")
endif()

set(ZeroMQ_ROOT "${CMAKE_CURRENT_LIST_DIR}/externals/${PLATFORM_ID}/libzmq/${BUILD_TYPE_DIR}")

find_package(ZeroMQ REQUIRED CONFIG)

add_executable(server "${CMAKE_CURRENT_LIST_DIR}/app/server.cpp")
target_link_libraries(server libzmq)
